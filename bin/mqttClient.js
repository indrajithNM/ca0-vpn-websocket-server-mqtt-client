const convention = require('./convention');
const websocketserver = require('./websocketserver');
const mqtt = require('mqtt')
const mqttclient = mqtt.connect(convention.getMqttUrl());
const MongoClient = require('mongodb').MongoClient; //mongodb
const url = convention.getMongodbURL(); // Connection mongodb URL
const dbName = convention.getMongodbName(); // Database Name

function client() {
    let topic = convention.getTopic();
    mqttclient.on('connect', function() {
        mqttclient.subscribe(topic, function(err) {
            if (!err) {
                console.log(topic)
            }
        })
    })
    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        const db = client.db(dbName);
        mqttclient.on('message', function(topic, message) {
            message = message.toString();
            //console.log(message)
            try {
                let obj = JSON.parse(message);
                // console.log(Object.keys(obj));
                //console.log(obj);
                updatedb(obj);
                // console.log(JSON.stringify(obj, null, 2));
            } catch (e) {
                console.error(e); // error in the above string (in this case, yes)!
            }

            // message is Buffer
            // Use connect method to connect to the server


            function updatedb(data) {
                db.collection(convention.getCollectionName("data")).insertOne(data, function(err, res) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(data);
                        websocketserver.informClients('Database updated');
                    }
                })


            }
        });
    })
}
client();