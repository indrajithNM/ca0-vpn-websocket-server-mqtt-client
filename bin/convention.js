const config = require('./config');
const getSecret = () => {
    return config.secret;
}
const getExpressPort = () => {
    return config.expressPort;
}
const getWSPort = () => {
    return config.webSocketPort;
}
const getMqttUrl = () => {
    if (config.mqttUserName != null && config.mqttPassword != null) {
        return `mqtt://${config.mqttUserName}:${config.mqttPassword}@${config.mqttURL}`;
    }
    return config.mqttUrl;
}
const getTopic = () => {
    return config.topic;
}
const getMongodbURL = () => {
    return config.mongodbURL;
}
const getMongodbName = () => {
    return config.mongodbName;
}
const getCollectionName = (collection) => {
    if (collection == 'data') {
        return config.dataCollection;
    }
    if (collection == 'nodes') {
        return config.nodesCollection;
    }
    if (collection == 'users') {
        return config.usersCollection;
    }
}
module.exports = {
    getExpressPort: getExpressPort,
    getSecret: getSecret,
    getWSPort: getWSPort,
    getMqttUrl: getMqttUrl,
    getTopic: getTopic,
    getMongodbURL: getMongodbURL,
    getMongodbName: getMongodbName,
    getCollectionName: getCollectionName
}