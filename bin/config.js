const expressPort = '4000';
//auth
const secret = 'something';
//websocket
const webSocketPort = '8080';
//mqtt
const mqttURL = 'mqtt.webyfy.com';
const mqttUserName = 'sammy';
const mqttPassword = 'p@ssword';
const topic = 'happy/#';
//mongodb
const mongodbURL = 'mongodb://localhost:27017';
const mongodbName = 'messages';
const dataCollection = 'documents';
const nodesCollection = 'nodes';
const usersCollection = 'users';
module.exports = {
    expressPort: expressPort,
    secret: secret,
    webSocketPort: webSocketPort,
    mqttURL: mqttURL,
    mqttUserName: mqttUserName,
    mqttPassword: mqttPassword,
    topic: topic,
    mongodbURL: mongodbURL,
    mongodbName: mongodbName,
    dataCollection: dataCollection,
    nodesCollection: nodesCollection,
    usersCollection: usersCollection
}