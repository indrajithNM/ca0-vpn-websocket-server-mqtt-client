const MongoClient = require('mongodb').MongoClient;
const convension = require('./convention');
const Strategy = require('passport-http').BasicStrategy;
const passport = require('passport');
const bcrypt = require('bcrypt');

function passportconf() {
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            res.status(200).json({ "Error": err.message })
        } else {
            passport.use(new Strategy(
                function(username, password, done) {
                    const db = client.db(convension.getMongodbName());
                    db.collection(convension.getCollectionName('users')).findOne({ id: username }, function(err, user) {
                        if (err) { return done(err); }
                        if (!user) { return done(null, false); }
                        bcrypt.compare(password, user.hash, function(err, res) {
                            if (res) {
                                console.log(user.id + " logged in");
                                return done(null, user);

                            } else {
                                return done(null, false);
                            }
                        });
                        // if (user.password != password) { return done(null, false); }
                        // console.log("log in success");
                        // return done(null, user);
                    });
                }
            ));
        }
    });
}
module.exports = {
    passportconf: passportconf
}