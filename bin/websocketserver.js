const convention = require('./convention');
const wsClients = [];
let wsClientIndex = -1;

function wsServer() {
    var WebSocketServer = require('websocket').server;
    var http = require('http');

    var server = http.createServer(function(request, response) {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });
    server.listen(convention.getWSPort(), function() {
        console.log((new Date()) + ' Websocket Server is listening on port ' + convention.getWSPort());
    });

    wsServer = new WebSocketServer({
        httpServer: server,
        // You should not use autoAcceptConnections for production
        // applications, as it defeats all standard cross-origin protection
        // facilities built into the protocol and the browser.  You should
        // *always* verify the connection's origin and decide whether or not
        // to accept it.
        autoAcceptConnections: false
    });

    function originIsAllowed(origin) {
        // put logic here to detect whether the specified origin is allowed.
        return true;
    }

    wsServer.on('request', function(request) {
        if (!originIsAllowed(request.origin)) {
            // Make sure we only accept requests from an allowed origin
            request.reject();
            console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
            return;
        }

        wsClientIndex++;
        wsClients[wsClientIndex] = request.accept('echo-protocol', request.origin);
        console.log((new Date()) + ' Connection accepted.');
        wsClients[wsClientIndex].on('message', function(message) {
            if (message.type === 'utf8') {
                console.log('Received Message: ' + message.utf8Data);
                for (let i = 0; i < wsClients.length; i++) {
                    wsClients[i].sendUTF('Received Message: ' + message.utf8Data);
                }
            } else if (message.type === 'binary') {
                console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                wsClients[wsClientIndex].sendBytes(message.binaryData);
            }
        });
        wsClients[wsClientIndex].on('close', function(reasonCode, description) {
            console.log((new Date()) + ' Peer ' + wsClients[wsClientIndex].remoteAddress + ' disconnected.');
        });
    });
}

function informClients(message) {
    for (let i = 0; i < wsClients.length; i++) {
        wsClients[i].sendUTF(message);
    }
}
wsServer();

module.exports = {
    informClients: informClients
}