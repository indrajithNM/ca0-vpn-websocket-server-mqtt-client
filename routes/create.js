const MongoClient = require('mongodb').MongoClient;
const websocketserver = require('../bin/websocketserver');
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const convension = require('../bin/convention');

router.use(bodyParser.urlencoded({ extended: true }));

/* POST new client. */
router.post('/', function(req, res, next) {
    function sendRespose(msg) {
        res.status(200).json(msg);
    }
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            sendRespose({ "Error": err.message });
            return;
        }
        var db = client.db(convension.getMongodbName());
        db.collection(convension.getCollectionName('nodes')).insertOne(req.body, function(err, res) {
            if (err) {
                sendRespose({ "Error": err.message });
                return;
            }

            if (res.ops != null) {
                console.log({
                    "Message": "Created node",
                    "data": res.ops
                });
                sendRespose({
                    "Message": "Created node",
                    "data": res.ops
                });
                websocketserver.informClients({
                    "type": "Post",
                    "Message": "Created node",
                    "data": res.ops
                });
            }
            console.log("1 document inserted");
        });
    });
});
module.exports = router;