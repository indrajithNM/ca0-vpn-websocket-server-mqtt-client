const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const convension = require("../bin/convention");

router.get('/:initialEpoch?/', function(req, res, next) {
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            res.status(200).json({ "Error": err.message })
        } else {
            let initialEpoch = req.params.initialEpoch;
            if (initialEpoch == null) {
                initialEpoch = Math.floor(new Date().getTime() / 1000) - (24 * 3600);
            }
            initialEpoch = Number(initialEpoch);
            let query1 = { Epoch: { $gte: initialEpoch } }
            const db = client.db(convension.getMongodbName());
            db.collection(convension.getCollectionName("data")).distinct('id', query1, function(err, result) {
                if (err) {
                    res.status(200).json({ "Error": err.message })
                }
                console.log(result);
                let query2 = { id: { $nin: result } };
                db.collection(convension.getCollectionName("nodes")).distinct('id', query2, function(err, result) {
                    if (err) {
                        res.status(200).json({ "Error": err.message })
                    }
                    console.log(result.length);
                    res.status(200).json({ "count": result.length, "data": result });
                });
            });
        }
    });
});
module.exports = router;