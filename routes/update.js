const MongoClient = require('mongodb').MongoClient;
const convension = require('../bin/convention')
const websocketserver = require('../bin/websocketserver');
const express = require('express');
const router = express.Router();

/* delete client. */
router.put('/:id', function(req, res, next) {
    function sendRespose(msg) {
        res.status(200).json({ msg });
    }
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            sendRespose({ "Error": err.message });
            return;
        }
        var db = client.db(convension.getMongodbName());
        let query = { id: req.params.id }
        let values = { $set: req.body }
        db.collection(convension.getCollectionName('nodes')).updateOne(query, values, function(err, res) {
            if (err) {
                sendRespose({ "Error": err.message });
                return;
            }
            console.log("1 document updated \n updated node");
            sendRespose({
                "Message": res.modifiedCount + " node(s) Updated",
                "node": req.params.id
            });
            if (res.modifiedCount !== 0) {
                websocketserver.informClients({
                    "type": "Put",
                    "Message": res.modifiedCount + " node(s) Updated",
                    "node": req.params.id
                });
            }

        });
    });
});
module.exports = router;