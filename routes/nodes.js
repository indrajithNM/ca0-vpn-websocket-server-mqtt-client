const MongoClient = require('mongodb').MongoClient;
const convension = require('../bin/convention');
const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();

/* GET users list. */
router.get('/', function(req, res, next) {
    function sendRespose(msg) {
        res.status(200).json({ "nodes": msg });
    }
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            console.error(err.message);
            sendRespose(err.message);
            return;
        }
        const db = client.db(convension.getMongodbName());
        db.collection(convension.getCollectionName("nodes")).find().toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            sendRespose(result);
            return;
        });
    });
});

//get user details with clientid
router.get('/:id', function(req, res, next) {
    function sendRespose(msg) {
        res.status(200).json({ "node": msg });
    }
    let nodeid = req.params.id;
    var query = { id: nodeid };

    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            console.error(err.message);
            sendRespose(err.message);
            return;
        }
        const db = client.db(convension.getMongodbName());
        db.collection(convension.getCollectionName("nodes")).find(query).toArray(function(err, result) {
            if (err) {
                console.error(err.message);
                sendRespose(err.message);
                return;
            }
            console.log(result);
            sendRespose(result);
            return;
        });
    });
});

module.exports = router;