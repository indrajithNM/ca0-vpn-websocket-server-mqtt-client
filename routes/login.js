const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportconf = require("../bin/usepassport");
const convension = require("../bin/convention");
let jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

passportconf.passportconf();

router.get('/', passport.authenticate('basic', { session: false }), function(req, res, next) {
    console.log(req.user.id)
    let token = jwt.sign({ username: req.user.id },
        convension.getSecret(), {
            expiresIn: '365d'
        }
    );
    res.json({
        success: true,
        message: 'Authentication successful!',
        token: token
    });
});
module.exports = router;