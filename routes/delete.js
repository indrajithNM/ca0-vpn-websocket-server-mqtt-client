const MongoClient = require('mongodb').MongoClient;
const convension = require('../bin/convention')
const websocketserver = require('../bin/websocketserver');
const express = require('express');
const router = express.Router();

/* delete client. */
router.delete('/:id', function(req, res, next) {
    function sendRespose(msg) {
        res.status(200).json(msg);
    }
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            sendRespose({ "Error": err.message });
            return;
        }
        var db = client.db(convension.getMongodbName());
        let query = { id: req.params.id }
        db.collection(convension.getCollectionName('nodes')).deleteOne(query, function(err, res) {
            if (err) {
                sendRespose({ "Error": err.message });
                return;
            }
            if (res.deletedCount != 0) {
                console.log({
                    "Message": "Deleted node",
                    "data": req.params.id
                });
                sendRespose({
                    "Message": "Deleted node",
                    "data": req.params.id
                });
                websocketserver.informClients({
                    "type": "Delete",
                    "Message": "Deleted node",
                    "data": req.params.id
                });
            }
        });
    });
});
module.exports = router;