const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const convension = require("../bin/convention");

router.get('/:initialEpoch?/:endEpoch?', function(req, res, next) {
    MongoClient.connect(convension.getMongodbURL(), { useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
        if (err) {
            res.status(200).json({ "Error": err.message })
        } else {
            let initialEpoch = req.params.initialEpoch;
            let endEpoch = req.params.endEpoch;
            if (endEpoch == null) {
                let d1 = new Date();
                d1.toUTCString();
                endEpoch = Math.floor(d1.getTime() / 1000);
            }
            endEpoch = Number(endEpoch);
            if (initialEpoch == null) {
                initialEpoch = endEpoch - (24 * 3600);
            }
            initialEpoch = Number(initialEpoch);
            let query = { $and: [{ Epoch: { $gte: initialEpoch } }, { Epoch: { $lte: endEpoch } }] }
            var db = client.db(convension.getMongodbName());
            db.collection(convension.getCollectionName("data")).find(query).toArray(function(err, result) {
                if (err) throw err;
                console.log(result.length);
                res.status(200).json({ "count": result.length, "data": result });
            });
        }
    });
});
module.exports = router;